// Collect array
var noArr = [];
document.querySelector("#btnSaveArr").onclick = function saveArr() {
    // input: arr các số nguyên
    var arrNo = convertIntArrById("#arrNo");
    // output
    document.querySelector("#arrNo").value = null;
    noArr.push(arrNo);
    document.querySelector("#ketQua").innerHTML = noArr;
}
// Bài 1
document.querySelector("#btnBai1").onclick = function() {
    // input
    // output
    var Sum = 0;
    // process
    noArr.forEach(element => {
        var number = element;
        if(number>=0){
            Sum += element;
        }
    });
    document.querySelector("#ketQua1").innerHTML = `Tổng số dương: ${Sum}`;
}
// Bài 2
document.querySelector("#btnBai2").onclick = function() {
    // input
    // output 
    var count = 0;
    // process
    noArr.forEach(item => {
        if(item>0){
            count++;
        }
    });
    document.querySelector("#ketQua2").innerHTML = `Số dương: ${count}`;
    
}

// Bài 3:
document.querySelector("#btnBai3").onclick = function() {
    // input
    // output
    var min = noArr[0];
    // process
    noArr.forEach(item => {
        if(item<min)
        {
            min=item;
        }
    })
    document.querySelector("#ketQua3").innerHTML = `Số nhỏ nhất: ${min}`;
}
// Bài 4: 
document.querySelector("#btnBai4").onclick = function() {
    // input
    var posNo = [];
    // output
    var result = '';
    // process
    noArr.forEach(item => {
        if(item>0){
            posNo.push(item);
        }
    })
    var min = posNo[0];
    var newArrLength = posNo.length;
    if(newArrLength==0)
    {
        result = 'Không có số dương trong mảng';
    } else {
        posNo.forEach(item => {
            if(item<min)
            {
                min = item;
            }
        })
        result = `Số dương nhỏ nhất: ${min}`;
    }
    document.querySelector("#ketQua4").innerHTML = result;
}

// Bài 5:
document.querySelector("#btnBai5").onclick = function() {
    // input
    // output
    var evenNo = noArr[0];
    // process
    noArr.forEach(item => {
        if(item%2==0 && item != 0){
            evenNo = item;
        }
    })
    if(evenNo%2!=0){
        evenNo = -1;
    }
    document.querySelector("#ketQua5").innerHTML = `Số chẵn cuối cùng: ${evenNo}`;
}
// Bài 6:
document.querySelector("#btnBai6_1").onclick = function() {
    // input
    var index1st = document.querySelector("#index1").value*1;
    var index2st = document.querySelector("#index2").value*1;
    var lengthArr = noArr.length;
    // output: mảng đã được hoán vị
    // process
    // kiểm tra index input có lớn hơn index mảng
    if(index1st>=lengthArr || index2st>=lengthArr)
    {
        return alert('chỉ số index nhập vào không hợp lệ');
    }
    // Hoán vị cho 2 giá trị
    var temp1 = noArr[index1st];
    noArr[index1st] = noArr[index2st];
    noArr[index2st] = temp1;
    document.querySelector("#ketQua6").innerHTML = printArray(noArr);
}
// Bài 7:
document.querySelector("#btnBai7").onclick = function() {
    // input
    // output
    var numberArrSorted = ``;
    // process
    numberArrSorted = sortByBubbleSort(noArr,noArr.length);
    document.querySelector("#ketQua7").innerHTML = `Mảng sau khi sắp xếp: ${numberArrSorted}`;
}
console.log("noArr7", noArr);
// Bài 8
document.querySelector("#btnBai8").onclick = function() {
    // input
    // output
    var result8 = 0;
    // process
    var isPrime = false;
    for(var i = 0; i<noArr.length;i++)
    {
        isPrime = checkPrimeNo(noArr[i]);
        if(isPrime){
            result8 = noArr[i];
            break;
        }
    }
    if(!isPrime){
        result8 = -1;
    }
    document.querySelector("#ketQua8").innerHTML = result8;
}
function checkPrimeNo(number)
{
    // process
    if(number<=1)
    {
        return false;
    }
    squareNo = Math.sqrt(number);
    for(var asc = 2; asc<=squareNo; asc++)
    {
        if(number%asc==0){
            return false;
        }
    }
    return true;
}
// Bài 9
var noArr9 = [];
document.querySelector("#btnBai9").onclick = function() {
    // input 
    var arrNo = convertFloatArrById("#arrNo9");
    // output: string arr, arr
    noArr9.push(arrNo);
    document.querySelector("#arrNo9").value = null;
    // process
    document.querySelector("#ketQua9").innerHTML = noArr9;
}
document.querySelector("#btnBai9_1").onclick = function() {
    // input: noArr9
    // ouput: số nguyên
    // process
    var countInt = 0;
    noArr9.forEach(item => {
        if(Number.isInteger(item)){
            countInt++;
        }
    })
    document.querySelector("#ketQua9_1").innerHTML = countInt;
}
// Bài 10
document.querySelector("#btnBai10").onclick = function() {
    // input
    // output: ><
    var compare = '';
    var countPosNo = 0;
    var countNegNo = 0;
    // process
    noArr.forEach(item=>{
        if(item>0){
            countPosNo++;
        }
        else{
            countNegNo++;
        }
    })
    if(countPosNo > countNegNo){
        compare = ">";
    } else if(countPosNo==countNegNo) {
        compare = "=";
    } else {
        compare = "<";
    }
    document.querySelector("#ketQua10").innerHTML = `Số dương ${compare} số âm`;
}