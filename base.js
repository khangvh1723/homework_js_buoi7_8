// function 
function convertIntArrById(idNumber){
    // input
    var numberInt = document.querySelector(idNumber).value*1;
    return numberInt;
}
function convertFloatArrById(idNumber){
    var numberStr = document.querySelector(idNumber).value;
    var numberFloat = parseFloat(numberStr);
    return numberFloat;
}
// Bài 6
function printArray(numberArr) {
    var numberArrStr = `<tr><th>index</th>`;
    for(var index = 0;index < numberArr.length;index++)
    {
        numberArrStr += `<th>${index}</th>`;
    }
    numberArrStr += `</tr><tr><td>value</td>`;
    numberArr.forEach(item => {
        numberArrStr += `<td>${item}</td>`;
    })
    numberArrStr+=`</tr>`;
    return numberArrStr;
}
function sortByBubbleSort(noArrSort,length)
{
    var temp = 0;
    for (var i = 0; i < length; i++){
        for (var j = 0; j < length -1 - i; j++){
            if (noArrSort[j] > noArrSort[j+1]){
                        temp = noArrSort[j];
                        noArrSort[j] = noArrSort[j+1];
                        noArrSort[j+1] = temp;
                }
            }
        }
        return noArrSort;
}